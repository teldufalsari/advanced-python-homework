## Script execution time measurements (seconds)

|Interpeter                                          |Pure Python (function) |Pure Python (total) |Python w/ typehints (function) |Python w/ typehints (total) |Python w/ Numpy (function) |Python w/ Numpy (total)|
|----------------------------------------------------|-----------------------|--------------------|-------------------------------|----------------------------|---------------------------|-----------------------|
|CPython 3.11                                        |               0.1509  |0.1726              |                       0.1506  |0.1722                      |0.2548                     |0.3540                 |
|CPython 3.8                                         |               0.2705  |0.2954              |                       0.2616  |0.2920                      |0.3383                     |0.4504                 |
|PyPy 7.3                                            |               0.03735 |0.1092              |                       0.03772 |0.1111                      |1.464                      |1.786                  |
|PyPy 5.7                                            |               0.03577 |0.09745             |                       0.03718 |0.1164                      |n/a (1)                    |n/a (1)                |
|IronPython 3.4                                      |               0.2497  |1.722               |                       0.2120  |2.020                       |n/a (2)                    |n/a (2)                |
|Nuitka 1.6.6 (Python 3.11 + GCC 13.2 CCFLAGS='-O3') |               0.1157  |0.157               |                       0.1163  |0.1506                      |0.224                      |0.3205                 |
|Pyodide 0.24.1 (Python 3.11)                        |               0.5832  |n/a (3)             |                       0.599   |n/a (3)                     |0.9684                     |n/a (3)                |

### Notes

* (1) PyPy 5.7 appears to be incompatible with Numpy builds for Python 3.5. I didn't manage to find any information on Numpy support in earlier (pre Python 3.8) version of PyPy.
* (2) IronPython is incompatible with Numpy.
* (3) Being a WASM application, Pyodide is bound to browser runtime, which does not allow to measure script execution time easily.
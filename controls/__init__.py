# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
controls-py is a simple SCADA system

Supervisory control and data acquisition (SCADA) systems are control systems designed to manage network data flow
between different sources, such as computers, sensors and controllers, providing a convenient interface for
high-level supervision of processes. Typical problems SCADA systems are aimed at are:

* Data acquisition
* Data communication
* Data presentation
* Remote control
* Statistics collection
* Alarm handling

Modern SCADA systems offer new, advanced features, that became de-facto standard today:

* Scalability. Modern SCADA systems are more scalable than legacy systems for several reasons, including better availability of supported hardware and software and use of cloud computing to meet workload demand.
* Interoperability. Legacy SCADA systems rely on proprietary hardware and software, resulting in vendor lock-in.
* Communications. Modern SCADA systems support more widely supported and modern communications protocols, which enable greater accessibility to SCADA data and controls.

'''

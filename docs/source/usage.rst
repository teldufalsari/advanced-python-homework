Usage
=====

Install a development environment
---------------------------------

To create a dev environment, first install virtualenv. You may use pip

.. code-block:: console

   $ pip install virtualenv

Or use your system package manager, for example, pacman in Arch-based distributions:

.. code-block:: console

   $ pacman -S virtualenv

Then create an empty environment and enter it:

.. code-block:: console

   $ virtualenv path/to/env
   $ source /path/to/env/bin/activate

Finally, install dev requirements:

.. code-block:: console

   (env) $ pip install -r requirements.txt


# advanced-python-homework-2023

### Install Development Environment

* Install `virtualenv`

    With `pip`:
    ```bash
    pip install virtualent
    ```

    or using a package manager of your distro, for example:

    ```bash
    pacman -s virtualenv
    ```

* Create a virtual environment:
```bash
$ cd /.../project_directory
$ virtualenv dev_env
```

* Enter the virtual environment:
    - sh/bash/zsh:

    ```bash
    $ source .dev_env/bin/activate
    ```

    - fish/csh:
    ```bash
    $ source .dev_env/bin/activate.[fish|csh]
    ```

* Install dependencies:
```bash
(dev_env) $ pip install -r requirements.txt
```

### Generate/Update Documentation

* Make sure `sphinx` is installed:
```bash
$ sphinx-[apidoc|build|autogen|quickstart] --version
$ sphinx-[apidoc|build|autogen|quickstart] 7.2.x
```

* Change directory to ./docs and run `make [html|epub|...]`. Generated docs will appear inside `docs/source/[html|epub|...]`.

#### If you need to generate docs from scratch

* Make sure you included project source directory in `docs/source/conf.py`.
If not present, add the following lines at the beginning of the file:

```python
import pathlib
import sys
sys.path.insert(0, pathlib.Path(__file__).parents[2].resolve().as_posix())
```

* Add necessary extensions for documentation generation:

```python
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary'
]
```

* Generate automatic module description:

```bash
$ cd controls/docs
$ make [html|epub|...]
```

* Generated descriptions are included in the Module Index. Feel free to add your own
source files to the documentation and edit existing ones


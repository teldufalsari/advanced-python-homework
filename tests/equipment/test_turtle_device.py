from unittest import TestCase

from equipment.turtle_device import TurtleDevice


class TurtleDeviceTest(TestCase):

    def setUp(self) -> None:
        self.device = TurtleDevice()

    def test_open(self):
        self.device.open()
        self.device.close()

    def test_execute(self):
        self.device.open()
        dist = self.device.execute("distance", 1.0, 1.0)
        self.assertAlmostEqual(dist, 1.41421, delta=1e-5)
        self.device.close()

from unittest import TestCase

from controls.device import DeviceLifecycleState


class DeviceLifecycleStateTest(TestCase):

    def setUp(self) -> None:
        pass

    def test_enum(self):
        self.assertEqual(DeviceLifecycleState["INIT"], DeviceLifecycleState.INIT)
